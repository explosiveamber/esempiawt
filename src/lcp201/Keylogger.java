/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lcp201;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 */
 /*
 * @author explosiveamber
 */
public class Keylogger extends Frame {
    
    private Panel pan;
    private StringBuffer buffer;
    
    public Keylogger(){
        
        
    
    
        this.buffer = new StringBuffer("Tasti premuti: ");
        this.setSize(300, 300);
        this.setTitle("LCP201 Trascina Mouse");
        this.setBackground(Color.lightGray);
        this.setLayout(null);
        this.setVisible(true);
        this.addWindowListener(new WindowAdapter(){
        
            public void windowClosing(WindowEvent we){
            
                System.exit(0);
            }
        });
        
        this.pan = new Panel();
        pan.setSize(250, 250);
        pan.addKeyListener(new Lettore(buffer));
        this.add(pan);
        pan.setVisible(true);
    
    }
    
    class Lettore extends KeyAdapter {
        
        private StringBuffer b;

        public StringBuffer getB() {
            return b;
        }
        
        public Lettore(StringBuffer b){
        
            this.b = b;
        }
    
        public void keyPressed(KeyEvent e){
        
            char premuto = e.getKeyChar();
            
            
            this.b.append(premuto); //side effect
            System.out.println(b.toString());
        }
    }

    
     public static void main(String[] args){
    
    
        Keylogger k = new Keylogger();
    
    }
    
}
