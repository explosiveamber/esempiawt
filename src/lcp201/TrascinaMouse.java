/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//package lcp201;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 */
 /*
 * @author explosiveamber
 */
public class TrascinaMouse extends Frame {
    
    class Trascinatore implements MouseMotionListener{

        @Override
        public void mouseDragged(MouseEvent e) {
           int x = e.getX();
           int y = e.getY();
           System.out.println(" " + x + ", " + y);
        }

        @Override
        public void mouseMoved(MouseEvent e) {
           
        }
        
       
    
    }
    
    private Panel pan;
    
    public TrascinaMouse(){
    
        this.setSize(300, 300);
        this.setTitle("LCP201 Trascina Mouse");
        this.setBackground(Color.lightGray);
        this.setLayout(null);
        this.setVisible(true);
        this.addWindowListener(new WindowAdapter(){
        
            public void windowClosing(WindowEvent we){
            
                System.exit(0);
            }
        });
        
        this.pan = new Panel();
        pan.setSize(250, 250);
        this.add(pan);
        pan.setVisible(true);
        
        
        pan.addMouseMotionListener(new Trascinatore());
    }
    
    public static void main(String[] args){
    
        TrascinaMouse t = new TrascinaMouse();
        
    }

   
}
