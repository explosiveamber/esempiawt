/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package esempiawt;

/**
 */
 /*
 * @author explosiveamber
 */

import java.awt.*;

public class Superficie extends Frame { // estendiamo la classe Frame 

    // dichiariamo 
    
    int x; // posizione orizzontale
    int y; // posizione verticale
    int w; // larghezza
    int h; // altezza
    
    public Superficie(int x, int y, int w, int h){
    
        this.x = x; // assegnamo pos.or.
        this.y = y; // assegnamo pos.ver.
        this.w = w; // assegnamo larghezza
        this.h = h; // assegnamo altezza

        this.setBounds(x, y, w, h); // settiamo le proprietà sulla finestra;
        this.setVisible(true); // settiamo a true la visibilità per mostrarla.
                               // metodo della classe Frame (superclasse di 
        // Superficie)
    }
    
    // nel main, chiamiamo il costruttore di Superficie per far apparire la fin.
    public static void main(String[] args){
         
        int mia_x = 200;
        int mia_y = 200;
        int mia_w = 400;
        int mia_h = 400;
        Superficie s = new Superficie(mia_x, mia_y, mia_w, mia_h);
    }
}
