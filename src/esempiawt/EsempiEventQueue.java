/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package esempiawt;

import java.awt.*;
import java.awt.event.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 */
 /*
 * @author explosiveamber
 */
public class EsempiEventQueue {
    
    
    
    
    public static void main(String[] args){
    /** Dimostreremo come creare eventi arbitrari e gestirli da jvm **/
    
        try {
            
            // Per costruire un esempio, istanziamo la nostra classe EvtThread
            
            EvtThread t1 = new EvtThread(new EventQueue());
            
            EvtThread t2 = new EvtThread(new EventQueue());
            
            // Un ActionEvent può avere come sorgente un semplice 'Object'
            
            ActionEvent act = new ActionEvent(new Object(), 1, "lallero");
            
            
            /* Dimostrazione di come possiamo usare quest'ActionEvent */
            
            t1.start();   // Possiamo far partire un thread e quindi
            
            t1.getEq().postEvent(act); // aggiungere eventi alla sua EventQueue
            
            System.out.println(t1.getEq().getNextEvent().toString()); //Eccolo!
            
            t1.join(); // Joiniamo l'Event Dispatcher Thread con il main thread
            
            //System.exit(0);
            
            
            //t2.start();
        } catch (InterruptedException ex) {
            Logger.getLogger(EsempiEventQueue.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    

}
