/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package esempiawt;

/**
 */
 /*
 * @author explosiveamber
 */
import java.awt.*;
import java.awt.event.*;



public class EsempioDueColori extends Frame {
    
    public EsempioDueColori(){
        
        this.addWindowListener(new WindowAdapter(){
        
            public void windowClosing(WindowEvent e){
            
                System.exit(0);
            }
        });
        this.setSize(400, 400);
        this.setTitle("Due Panel");
        Panel p1 = new Panel();
        Panel p2 = new Panel();
        Color c1 = new Color(0,0,0,0);
        Color c2 = new Color(255, 255, 255, 0);
        p1.setBackground(c1);
        p2.setBackground(c2);
        this.setLayout(new GridLayout());
        this.add(p1);
        this.add(p2);
        p1.setVisible(true);
        p2.setVisible(true);
        this.setVisible(true);
    }

    public static void main(String[] args){
    
        EsempioDueColori edc = new EsempioDueColori();
    }
}
