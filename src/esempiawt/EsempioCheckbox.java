/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package esempiawt;

/**
 */
 /*
 * @author explosiveamber
 */

import java.awt.*;
import java.awt.event.*;

public class EsempioCheckbox extends Frame {

    public EsempioCheckbox(){
    
        this.addWindowListener(new WindowAdapter(){
        
            public void windowClosing(WindowEvent e){
            
                System.exit(0);
            }
        });
        this.setSize(400, 400);
        this.setTitle("Due Panel con pulsante Scambia Colori");
        
        Color nero = new Color(0,0,0,0);
        Color bianco = new Color(255, 255, 255, 0);
        
        Panel p1 = new Panel();
        Panel p2 = new Panel();
        
        p1.setBackground(nero);
        p2.setBackground(bianco);
        
        // Creiamo una seconda finestra con un panel e, attaccato, un checkbox
        Frame f = new Frame();
        f.setSize(120, 220);
        Panel p3 = new Panel();
        p3.setSize(100, 200);
        p3.setBackground(Color.PINK);
        f.add(p3);
        
        // Diamo un layout al panel, sebbene qui non fosse indispensabile
        p3.setLayout(new GridLayout(3,1));
        p3.setVisible(true);
        f.setVisible(true);
        
        
        // Checkbox
        Checkbox inverti = new Checkbox("Inverti...", false);
        inverti.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e) {
                
                /*
                Dobbiamo recuperare lo stato del checkbox (premuto/non premuto)
                Possiamo farlo sia con il metodo getItemState di ItemEvent
                che con il metodo getState di Checkbox.
                Recuperiamo il colore di sfondo e, se è nero, lo mettiamo a bianco e viceversa.
                */
                if(inverti.getState() == true){inversione(p1, p2, nero, bianco);} 
                else {inversione(p1, p2, bianco, nero);}
            }
        });
        
        
        
        inverti.setVisible(true);
        p3.add(inverti);
       
        
        this.setLayout(new GridLayout());
        
        this.add(p1);
        this.add(p2);
        
        
        p1.setVisible(true);
        p2.setVisible(true);
        
        this.setVisible(true);
    }
    
    // Occorre assegnare una logica al checkbox, meglio se in un metodo apposta
    private void inversione(Panel p1, Panel p2, Color c1, Color c2){ 
    
        if(p1.getBackground() == c1) p1.setBackground(c2);
        if(p2.getBackground() == c2) p2.setBackground(c1);
        
    }
    
    // Client di test
    public static void main(String[] args){
    
        EsempioCheckbox c = new EsempioCheckbox();
        System.out.print("Uso: cliccare più volte sul pulsante <Inverti>. "
        + "Se il pulsante non appare, chiudere e riaprire il programma.");
    }
}
